# KobanaClient

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/kobana_client`. To experiment with that code, run `bin/console` for an interactive prompt.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'kobana_client'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install kobana_client

## Usage

### Configure seu api key e user agend

```ruby
KobanaClient.api_key = SEU_TOKEN_DE_API

KobanaClient.user_agent = SEU_USER_AGENT

KobanaClient::Pix.create({
        payer: {name: "Nome do Cliente",
                document_number: "Numero do Documento"},
        amount: "Valor",
        pix_account_id: "Pix Account ID",
        expire_at: "Data Vencimento"
      })
```

## Test

```ruby
bundle exec rspec spec/
```
## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/kobana_client.
