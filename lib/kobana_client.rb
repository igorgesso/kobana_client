# frozen_string_literal: true

require_relative "kobana_client/version"
require_relative "kobana_client/charge"
require_relative "kobana_client/pix"

module KobanaClient
  class Error < StandardError; end
  # Your code goes here...

  class << self
    attr_accessor :token_api, :user_agent
  end

  def self.base_uri
    "https://api-sandbox.kobana.com.br/v2/"
  end
end
