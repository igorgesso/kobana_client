module KobanaClient
  module Charge
    module ClassMethods
      def create(body = {})
        conn = Faraday.new(
          url: KobanaClient.base_uri,
          headers: {"User-Agent" => KobanaClient.user_agent}
        ) do |f|
          f.request :authorization, "Bearer", -> { KobanaClient.token_api }
          f.request :json
          f.response :json
        end

        conn.post("charge/pix", body)
      end
    end

    def self.included(base)
      base.extend(ClassMethods)
    end
  end
end
