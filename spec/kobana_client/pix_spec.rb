# frozen_string_literal: true

require "spec_helper"
require "time"
require "pry"

RSpec.describe KobanaClient::Pix do
  describe ".create" do
    before(:all) do
      KobanaClient.token_api = "LCmgG2Sz7arUEqDdtIJxO7k2XCUczHFb4ifJ2KvooPk"
      KobanaClient.user_agent = "Igor Rezende <igorgesso@gmail.com>"
    end

    it "should create a charge with pix", :vcr do
      charge = KobanaClient::Pix.create({
        payer: {name: "Awesome Customer",
                document_number: "848.398.720-15"},
        amount: "10,33",
        pix_account_id: "59",
        expire_at: Time.now.iso8601
      })

      expect(charge.status).to eq(200)
      expect(charge.body["amount"]).to eq(10.33)
    end
  end
end
