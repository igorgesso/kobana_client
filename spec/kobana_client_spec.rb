# frozen_string_literal: true

RSpec.describe KobanaClient do
  it "has a version number" do
    expect(KobanaClient::VERSION).not_to be nil
  end
end
